FROM node:10-alpine

RUN mkdir /slides
WORKDIR /slides

COPY public public
COPY src src
COPY package.json package.json
COPY webpack.config.js webpack.config.js
RUN npm i
CMD npm run slides
