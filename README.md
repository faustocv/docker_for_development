# Docker for Development
By Continuous Delivery Community - TW

## Prerequisites

* NodeJS
* NPM

## Installation

Install dependencies:

```bash
$ npm install
```

## Execution

```bash
npm run slides
```

See slides in the following URL:

```bash
http://localhost:8080
```
