class: center, middle, subtitle-silver

# Docker for Development
## GEEK Nights UIO

By Luis Herrera, Armando Collazo, Cristian Toaquiza, Fausto Castañeda

### ThoughtWorks

---

class: center, middle, subtitle-blue

# Do you wanna follow slides?

.center[![qrcode](./assets/slides_qrcode.png)]

---

class: center, middle, subtitle-orange

# Código de conducta

---

class: common-content

## Código de conducta

* Espacio libre de acoso para todas las personas.

* El lenguaje sexual y las imágenes sexuales no son apropiadas en ningún lugar del evento.

* Ningún tipo de arma o droga está permitido.

* Estamos aquí para ayudarte.


---

class: center, middle, subtitle-green

# Agenda

---

class: common-content

## Agenda

* Concepts ( 45 min )
* Demo ( 30 min )
* Q & A ( 15 min )

---

class: center, middle, subtitle-orange

# Why this meetup?

---

class: center, middle, subtitle-blue

# How much time a code setup takes in your computer?

---

class: center, middle, subtitle-green

# How much time a code setup takes in your computer?
# 1 hour ?

---

class: center, middle, subtitle-green

# How much time a code setup takes in your computer?
# 8 hour ?

---

class: center, middle, subtitle-orange

# How much time a code setup takes in your computer?
# 5 days ?

---

class: center, middle, subtitle-orange

# How much time a code setup takes in your computer?
# 15 days ?

---

class: center, middle, subtitle-red

# How much time a code setup takes in your computer?
# > 15 days ?

---

class: center, middle, subtitle-silver

# And, what about your use Windows, Linux or Mac?

---

class: center, middle, subtitle-red

# And even worse each member in your team uses a different platform ...

---

class: center, middle, subtitle-silver

.center[![meme](./assets/works_in_my_machine.png)]

---

class: center, middle, subtitle-red

# So, who can save us?

---

class: center, middle, subtitle-silver

.center[![moby](./assets/moby.png)]

---

class: center, middle, subtitle-blue

# but, what is Docker?

---

class: common-content

## What is Docker?

.center[![Moby logo](./assets/moby_project_logo.png)]


The Docker term means:
* Docker (the company:  Docker Inc.).
* Docker (the commercial software products: Docker CE and Docker EE).
* Docker (the open source project, before April 2017).

Now, .standout-word[the open source project is called Moby.]

---

class: common-content

## What is Docker? (2)

.normal-word[Docker is a container technology]

.center[.key-word[to build, ship and run distributed applications.]]

.center[![Docker ecosystem](./assets/scaled-docker.png)]
.right[.content-source[https://www.docker.com/what-container]]

---

class: common-content

## What is Docker? (3) - Docker Architecture

Main components

| Name | Explanation |
|--- |--- |
| Docker engine / Docker daemon / Docker host | the runtime to run docker containers |
| Docker client | e.g. CLI, docker-api Ruby gem. |
| Container |  the isolated environment in which an application runs |
| Image | the snapshot of a container, inert and immutable. |
| Registry | a place to storage images. |

---

class: common-content

## What is Docker? (4) - Docker Architecture

.center[
  <img src="assets/docker_architecture.svg" alt="Docker architecture" style="width: 80%;">
]

.right[
  .content-source[https://docs.docker.com/introduction/understanding-docker/]
]

---

class: center, middle, subtitle-orange

# But, what I will gain using Docker for development?

---

class: common-content

## Benefits

.center[.key-word[Isolation] <span>&nbsp;&nbsp;</span> .key-word[Faster setup] <span>&nbsp;&nbsp;</span> .normal-word[Run anywhere]]

.center[.normal-word[No more dirty PC] <span>&nbsp;&nbsp;</span> .key-word[Better resource use]]

.center[.key-word[Faster deployment] <span>&nbsp;&nbsp;</span> .key-word[Easy migration] ]

.center[.normal-word[More than one version of the same stuff]]

---

class: common-content

## Constraints

.key-word[No easy integration with IDE]

.center[.normal-word[Extra computing layer] <span>&nbsp;&nbsp;</span> .key-word[More resources (ram, cpu) required]]

.right[.key-word[Comprehension of Docker architecture] <span>&nbsp;&nbsp;</span> .normal-word[Managing volumes]]


---

class: center, middle, subtitle-blue

# Let's look an example!

---

class: center, middle, subtitle-blue

# Compiling a Java code

---

class: common-content

## Compiling a Java code

* Traditional way with Maven

```bash
$ java -version
openjdk version "1.8.0_171"

$ mvn -version
maven version "3.0"

$ mvn clean install

```

* Docker way with Maven image

```bash
$ docker run --rm \
         -v /my_project:/src \
         -w /src \
         maven:3-jdk-8 \
         mvn clean install

```

---

class: center, middle, subtitle-blue

# Let's upgrade to Java 10. How?

---

class: common-content

## Compiling a Java code (2)

* Traditional way with Maven

```bash
$ sudo apt-get install oracle-java10-installer
$ sudo update-alternatives --config java

...

$ mvn clean install ?

```

* Docker way with Maven image

```bash
$ docker run --rm \
         -v /my_project:/src \
         -w /src \
         maven:3-jdk-10-slim \
         mvn clean install

```

---

class: center, middle, subtitle-silver

# Get ready for a demo!

---

class: center, middle, subtitle-orange

# TODOs web application!

---

class: common-content

## TODOs web application

.center[![qrcode](./assets/todos_arquitectura.jpg)]

---

class: common-content

## TODOs web application (2)

.center[![qrcode](./assets/todos_arquitectura_2.jpg)]

---

class: center, middle, subtitle-blue

# Let's code

---

class: center, middle, subtitle-silver

# Wrap up

---

class: center, middle, subtitle-orange

# External Resources

---

class: common-content

## External Resources

Books:

* [Docker para desenvolvedores](https://leanpub.com/dockerparadesenvolvedores), Rafael Gomes.

Posts:

* [How To Share Data between Docker Containers](https://www.digitalocean.com/community/tutorials/how-to-share-data-between-docker-containers), Melissa Anderson.

Online Courses:

* [Linux Academy](https://linuxacademy.com/), *Docker Deep Dive* course.

---

class: center, middle, subtitle-green

# Source code

---

class: common-content

## Source code

.center[
  <a href="https://github.com/TWGeekNights">
    <img src="assets/source_code.png" alt="Source code" style="width: 80%;">
  </a>
]

---

class: center, middle, subtitle-blue

# Q & A time

---

class: center, middle, subtitle-red

# Feedback time

---

.center[
  <a href="https://www.thoughtworks.com/careers/jobs">
    <img src="assets/recruting.png" alt="Recruting" style="width: 100%;" />
  </a>
]

---

class: common-content

## Join us

.center[
  <a href="https://events.docker.com/quito/">
    <img src="assets/docker_community.png" alt="Docker community" style="width: 100%;" />
  </a>
]

---

<img src="assets/next_meetup.png" alt="Next meetup" style="width: 100%;" />
