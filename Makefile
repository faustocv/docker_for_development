override IMAGE_TAG = dockerfordev


build_image:
	@docker build \
	  -t $(IMAGE_TAG):latest \
	  .

run_container:
	@docker run \
	  --rm \
	  -it \
	  --name dockerfordev-container \
	  -e PORT=8080 \
	  -p 8080:8080 \
	  $(IMAGE_TAG):latest

define deploy_to
	docker tag $(IMAGE_TAG):latest registry.heroku.com/dockergeek/web
	@docker login --username=_ --password=$(1) registry.heroku.com
	docker push registry.heroku.com/dockergeek/web
endef

deploy:
	$(call deploy_to,$(TOKEN))
